"""Score predicted classifications from multiple approaches based on a
gold-standard dataset. When used as a script, arguments must be provided
for these data sets in the form of CSV files.

"""

import argparse
import numpy
import os
import pandas
import sklearn.metrics as metrics
import time

from sklearn.preprocessing import LabelBinarizer

DELIM_LABEL_PAD_CHAR = "-"
DELIM_LABEL_WIDTH = 70

def normalize_string_columns(df, inplace=False):
    """Format column names and string data in a DataFrame for processing.

    Args:
        df (pandas.DataFrame): Data to format
        inplace (bool, optional): Indicates whether to operate on the DataFrame
            passed in or on a copy. Defaults to False.

    Returns:
        pandas.DataFrame: Reference to the data operated on.

    """
    if not inplace:
        df = df.copy()

    df.columns = df.columns.str.strip().str.lower()
    for col in df.columns[df.dtypes=='object']:
        df[col] = df[col].str.strip().str.lower()

    return df

def print_verbose_metrics(y_true, y_bin_pred, y_prob_pred, n_categories, category_labels):
    """Lengthy function to display macro-level results for each category in a
    classification problem.

    Args:
        y_true (numpy.ndarray): A binary matrix containing the true labels for a
            data set. The categories.
        y_bin_pred (numpy.ndarray): A collection of binary matrices of the same
            format as y_true, but containing a matrix for N data sets being
            evaluated. These matrices may contain more categories than y_true,
            which means these need to be filtered out in some metrics. The
            ordering of categories must match y_true with extras appened later.
        y_prob_pred (numpy.ndarray): Same as y_bin_pred, but probabilities for
            each class are stored rather than {0,1} predictions.
        n_categories (int): The number of standard categories. This should match
            the number of categories in y_true. Used to help filter out extra
            categories in y_*_pred.
        category_labels (dict): Keys are integers mapping to string labels for
            categories.

    Raises:
        ValueError: Check that dimensions of matrices are correct.

    """

    if len(y_bin_pred.shape) != len(y_prob_pred.shape) != 3:
        raise ValueError("Expected 3 dimensions for both y_bin_pred and y_prob_pred.")
    if n_categories != y_prob_pred.shape[-1]:
        print("n_categories={} != Class count y_prob_pred={}".format(
            n_categories, y_prob_pred.shape[-1]
        ))
        raise ValueError("y_bin_pred and y_prob_pred must have the same dimensions.")

    for i in range(len(y_bin_pred)):

        macro_df = pandas.DataFrame(list(metrics.precision_recall_fscore_support(
            y_true[:, 0:n_categories],
            y_bin_pred[i][:, 0:n_categories],
            # warn_for=tuple()
        ))).T

        macro_df.columns = ["Precision", "Recall", "F1", "Support"]

        # Compute macro-averages if verbose option is set.

        for j in range(n_categories):
            macro_df.loc[j, 'ROC AUC'] = metrics.roc_auc_score(
                y_true[:, j],
                y_prob_pred[i][:, j]
            )

        # This gives the macro ROC AUC score calculated from class averages.
        # This is also the default type for roc_auc_score().

        macro_df.rename(index=category_labels, inplace=True)

        macro_avgs = pandas.Series(index=macro_df.columns)
        macro_avgs["Precision"] = metrics.precision_score(
                y_true[:,0:n_categories],
                y_bin_pred[i][:,0:n_categories],
                average="macro"
        )
        macro_avgs["Recall"] = metrics.recall_score(
                y_true[:,0:n_categories],
                y_bin_pred[i][:,0:n_categories],
                average="macro"
        )
        macro_avgs["F1"] = metrics.f1_score(
                y_true[:,0:n_categories],
                y_bin_pred[i][:,0:n_categories],
                average="macro"
        )

        macro_avgs["Support"] = macro_df["Support"].sum() / len(macro_df["Support"])
        macro_avgs["ROC AUC"] = metrics.roc_auc_score(
            y_true[:,0:n_categories],
            y_prob_pred[i][:,0:n_categories],
            average="macro"
        )

        macro_df = macro_df.round(3)
        macro_avgs = macro_avgs.round(3)
        macro_avgs["Accuracy"] = ""
        macro_df.loc[""] = ""
        macro_df.loc["Macro Avgs."] = macro_avgs

        current_timestamp = time.time()
        readable_timestamp = time.ctime(current_timestamp)
        print()
        print("Macro-level Statistics (File {})".format(i+1).center(DELIM_LABEL_WIDTH, DELIM_LABEL_PAD_CHAR))
        print("Current Timestamp: " + str(readable_timestamp))
        print()
        print(macro_df)
        print()
        print("End Macro-level Statistics (File {})".format(i+1).center(DELIM_LABEL_WIDTH, DELIM_LABEL_PAD_CHAR))
        print()

def print_final_metrics(y_true, y_bin_pred, y_prob_pred, n_categories):
    """Lengthy function to display macro-level overall results for a
    classification problem.

    Args:
        y_true (numpy.ndarray): A binary matrix containing the true labels for a
            data set. The categories.
        y_bin_pred (numpy.ndarray): A collection of binary matrices of the same
            format as y_true, but containing a matrix for N data sets being
            evaluated. These matrices may contain more categories than y_true,
            which means these need to be filtered out in some metrics. The
            ordering of categories must match y_true with extras appened later.
        y_prob_pred (numpy.ndarray): Same as y_bin_pred, but probabilities for
            each class are stored rather than {0,1} predictions.
        n_categories (int): The number of standard categories. This should match
            the number of categories in y_true. Used to help filter out extra
            categories in y_*_pred.

    Raises:
        ValueError: Check that dimensions of matrices are correct.

    """

    if len(y_bin_pred.shape) != len(y_prob_pred.shape) != 3:
        raise ValueError("Expected 3 dimensions for both y_bin_pred and y_prob_pred.")
    if n_categories != y_prob_pred.shape[-1]:
        print("n_categories={} != Class count y_prob_pred={}".format(
            n_categories, y_prob_pred.shape[-1]
        ))
        raise ValueError("y_bin_pred and y_prob_pred must have the same dimensions.")

    # Obtain the scores by iterating over the data sets.

    score_df = pandas.DataFrame({
        # "Precision": numpy.zeros(len(y_bin_pred)),
        # "Recall": numpy.zeros(len(y_bin_pred)),
        "F1": numpy.zeros(len(y_bin_pred)),
        "Accuracy": numpy.zeros(len(y_bin_pred)),
        "ROC AUC": numpy.zeros(len(y_bin_pred))
    })

    for i, _ in enumerate(y_bin_pred):

        # score_df.loc[i,"Precision"] = metrics.precision_score(
        #     y_true[:, 0:n_categories],
        #     y_bin_pred[i][:, 0:n_categories],
        #     average="macro"
        # )
        # score_df.loc[i,"Recall"] = metrics.recall_score(
        #     y_true[:, 0:n_categories],
        #     y_bin_pred[i][:, 0:n_categories],
        #     average="macro"
        # )
        score_df.loc[i,"F1"] = metrics.f1_score(
            y_true[:, 0:n_categories],
            y_bin_pred[i][:, 0:n_categories],
            average="macro"
        )

        # Gives the accuracy based on completely matching class sets by transaction.
        # Again, no need to flatten the lists.

        score_df.loc[i, "Accuracy"] = metrics.accuracy_score(y_true, y_bin_pred[i])

        # Remove y columns for the non-standard categories.

        score_df.loc[i, "ROC AUC"] = metrics.roc_auc_score(
            y_true[:, 0:n_categories],
            y_prob_pred[i],
            average="macro"
        )

    score_df.index = ["File {}".format(ix+1) for ix in score_df.index]

    print()
    print("Results".center(DELIM_LABEL_WIDTH, DELIM_LABEL_PAD_CHAR))
    print("Current Timestamp: " + str(readable_timestamp))
    print()
    print(score_df)

    # Show the files numbers for the max metrics.
    print()
    print("File with the maximum:")
    print("{:>15}  {}".format("F1 Score:", score_df["F1"].idxmax()))
    print("{:>15}  {}".format("Accuracy Score:", score_df["Accuracy"].idxmax()))
    print("{:>15}  {}".format("ROC AUC Score:", score_df["ROC AUC"].idxmax()))
    print()
    print("F1 and ROC AUC scores use macro-average calculations.")
    print()
    print("End Results".center(DELIM_LABEL_WIDTH, DELIM_LABEL_PAD_CHAR))
    print()

def main(args):

    file_paths = [args.gs_file, *args.files]

    for path in file_paths:
        if not os.path.isfile(path):
            raise FileNotFoundError("File {} does not exist.".format(path))
        if not path.lower().endswith("csv"):
            raise Exception("File {} does not contain a 'csv' file extension.".format(
                path
            ))

    fixed_dtype = {"id": str, "category": int}

    y, *X = [pandas.read_csv(path, dtype=fixed_dtype) for path in file_paths]

    normalize_string_columns(y, inplace=True)
    for x in X: normalize_string_columns(x, inplace=True)

    y.drop_duplicates(["id"], inplace=True)
    for x in X: x.drop_duplicates(["id"], inplace=True)

    y["category"] = y["category"].astype("category")
    for x in X: x["category"] = x["category"].astype("category")

    standard_categories = y["category"].cat.categories
    n_standard_categories = len(standard_categories)
    other_categories = set()
    for x in X:
        other_categories |= set(
            x["category"].cat.categories.difference(y["category"].cat.categories)
        )
    all_categories = set(standard_categories) | other_categories

    y["category"].cat.add_categories(
        all_categories - set(y["category"].cat.categories),
        inplace=True
    )
    for x in X:
        x["category"].cat.set_categories(
            y["category"].cat.categories,
            ordered=True,
            inplace=True
        )

    for i, x in enumerate(X):
        # Categories must match with y.
        if len(y["category"].cat.categories.symmetric_difference(
            x["category"].cat.categories)) != 0:
            raise ValueError(
                "There is a mismatch in categories between y and X[{}].".format(i)
            )
        # Transaction IDs should also match with y.
        if len(set(y["id"]) ^ set(x["id"])) != 0:
            raise ValueError(
                "The set of transactions between the gold-standard file and " +\
                "file {} do not match.".format(i+1)
            )

    # # Encode data
    #
    # We have to use a list of lists representing binarized categories for each
    # transaction in order to use Scikit Learn's metrics module.

    id_forward_transl = {lbl:ix for ix, lbl in enumerate(y["id"].unique())}
    # id_inverted_transl = {ix:lbl for lbl, ix in id_forward_transl.items()}

    category_forward_transl = {lbl:ix for ix, lbl in enumerate(y["category"].cat.categories)}
    category_inverted_transl = {ix:lbl for lbl, ix in category_forward_transl.items()}

    # Replace the IDs in the original DataFrame.\

    y["id"].replace(id_forward_transl, inplace=True)
    for x in X: x["id"].replace(id_forward_transl, inplace=True)

    y["category"].cat.rename_categories(category_forward_transl, inplace=True)
    for x in X: x["category"].cat.rename_categories(category_forward_transl, inplace=True)

    y = y.set_index("id").sort_index()
    for i, _ in enumerate(X): X[i] = X[i].set_index("id").sort_index()

    lb = LabelBinarizer()
    lb.fit(y["category"].cat.categories)

    y_bin = lb.transform(y["category"])
    X_bin = numpy.array([lb.transform(x["category"]) for x in X])

    # Instead, we have probabilities for data sets in X. Convert these to matrices.
    # Note that there is no probability given for "Other" in the spec.

    X_prob = numpy.array([x.iloc[:,1:].values for x in X])

    # Display the following if the verbose option is flagged.
    # Get the macro-level averages. Exclude "other" categories from this analysis.

    if args.verbose:

        print_verbose_metrics(y_bin, X_bin, X_prob, n_standard_categories, category_inverted_transl)

    print_final_metrics(y_bin, X_bin, X_prob, n_standard_categories)

if __name__ == "__main__":

    argument_parser = argparse.ArgumentParser(
        description="Script for checking the results of two classification " + \
        "models based on a gold standard dataset. Input files should be in CSV " + \
        "format."
    )

    argument_parser.add_argument(
        'gs_file',
        type=str,
        help="Path to the gold standard file.",
    )

    argument_parser.add_argument(
        'files',
        nargs="+",
        type=str,
        help="Paths to all files classification results for comparison.",
    )

    argument_parser.add_argument(
        '-v',
        "--verbose",
        action="store_true",
        help="Show category-level metrics along with the overall results.",
    )

    args = argument_parser.parse_args()

    main(args)
